@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Chat Room</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @forelse($messages as $message)
                        {{ $message->message }}
                        <p> By : {{ $message->sender->name }} </p>
                        <br>
                    @empty
                        <p> There is no messages in this conversation.</p>
                    @endforelse

                    {!! Form::open(['route' => 'message']) !!}
                        {{ Form::token() }}
                        {{ Form::hidden('conversation_id', 2) }}
                        {{ Form::label('message' , 'Message') }}
                        {{ Form::text('message')}}
                        
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
