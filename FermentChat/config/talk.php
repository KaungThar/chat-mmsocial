<?php

return [
    'user' => [
        'model' => 'App\User',
    ],
    'broadcast' => [
        'enable' => true,
        'app_name' => 'mmsocialchat',
        'pusher' => [
            'app_id' => '556104',
            'app_key' => '83c0eac44b65d4ea93a0',
            'app_secret' => '7517c7d0fef452a25609',
            'options' => [
                'cluster' => 'ap1',
                'encrypted' => true
            ]
        ],
    ],
];
