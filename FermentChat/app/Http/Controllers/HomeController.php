<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Talk;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
      
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Talk::user(Auth::user()->id);
        $conversationId = 2;
        $conversations = Talk::getConversationsById($conversationId);
        $messages = $conversations->messages;
        return view('home', compact('messages'));
    }

    public function message(Request $request)
    {
        $user = Talk::user(Auth::user()->id);
        $user->sendMessage($request->conversation_id, $request->message);
        return redirect()->route('home');
    }
}
